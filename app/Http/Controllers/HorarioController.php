<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Horario_Atencion;
use Illuminate\Support\Facades\Validator;

class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $horariosprueba = Horario_Atencion::all();
        return view ("TestHorarios.index", compact ("horariosprueba"));
       // return $horariosprueba;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('TestHorarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator :: make ($request->all(),[
            "dia_atencion" => "date_format:Y-m-d",
            "hora_cierre" => "required",
            "hora_apertura" => "required",
            "test1_validation" => "email",
            "test2_validation" => "alpha_num",
            "test3_validation" => "alpha",
            "test4_validation" => "integer",

        ]);

        if($v->fails())
        {

                return redirect () -> back () -> withErrors($v->errors())->withInput();

           }else {

            Horario_Atencion::create($request->all());
            return redirect()->route('horariosprueba.index')->with('success','Horario creado exitosamente!!');

           }

       

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Horario_Atencion $horariosprueba)
    {
        return view('TestHorarios.edit', compact ("horariosprueba"));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $horariosprueba = Horario_Atencion :: find ($id);
        $horariosprueba -> update ($request -> all());
        return redirect () -> route ("horariosprueba.index") -> with ("success","Horario modificado con exito");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $horariosprueba = Horario_Atencion :: find ($id);
        $horariosprueba -> delete ();
        return redirect () -> route ("horariosprueba.index") -> with ("success","Horario eliminado con exito");

    }
}
