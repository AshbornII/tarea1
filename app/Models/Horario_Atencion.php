<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horario_Atencion extends Model
{
    use HasFactory;
    protected $table = "horario";
    protected $primarykey = "id";
    public $timestamps = false;
    protected $fillable = [

        "id",
        "hora_apertura",
        "hora_cierre",
        "dia_atencion",
        "test1_validation",
        "test2_validation",
        "test3_validation",
        "test4_validation",
        "cedula_test",
    ];
}
