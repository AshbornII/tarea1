<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('menu_id_menu')->index('fk_user_has_menu_menu1_idx');
            $table->integer('clientes_id_cliente')->index('fk_user_has_menu_clientes1_idx');
            $table->integer('mesas_id_mesa')->index('fk_user_has_menu_mesas1_idx');
            $table->string('tipo_de_pedido', 45);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
};
