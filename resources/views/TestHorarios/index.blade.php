@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE HORARIOS DE RESERVAS</h1>
        <div>
            <a class="btn btn-success" href="{{ route('horariosprueba.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Dia Atención</th>
                <th>Hora De Apertura</th>
                <th>Hora De Cierre</th>
                <th>Telefono</th>
                <th>Test 1</th>
                <th>Test 2</th>
                <th>Test 3</th>
                <th>Test 4</th>
                <th>Botones</th>
            </tr>
            @foreach ($horariosprueba as $horario)
                <tr>
                    <td>{{ $horario->id }}</td>
                    <td>{{ $horario->dia_atencion }}</td>
                    <td>{{ $horario->hora_apertura }}</td>
                    <td>{{ $horario->hora_cierre }}</td>
                    <td>{{$horario->cedula_test}}</td>
                    <td>{{ $horario->test1_validation }}</td>
                    <td>{{ $horario->test2_validation }}</td>
                    <td>{{ $horario->test3_validation }}</td>
                    <td>{{ $horario->test4_validation }}</td>

                    <td><a class="btn btn-primary" href="{{ route('horariosprueba.edit',$horario) }}">
                        <i class="fa-solid fa-pen-to-square"></i>
                    </a>
                    <form action="{{ route('horariosprueba.destroy',$horario->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
</div>
@endsection
