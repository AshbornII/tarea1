@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Horario</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('horariosprueba.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('horariosprueba.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Dia de atencion:</strong>
                        <input type="date" name="dia_atencion" class="form-control"
                            placeholder="Ingresar el dia de su reserva" value="{{old("dia_atencion")}}" required>
                        @error( "dia_atencion")
                        <p>{{$message}}</p>
                        @enderror


                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora de apertura:</strong>
                        <input type="time" name="hora_apertura" class="form-control"
                            placeholder="Ingresar la hora inicial de su reserva"value="{{old("hora_apertura")}}" required>

                            @error( "hora_apertura")
                        <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Hora de cierre:</strong>
                        <input type="time" name="hora_cierre" class="form-control"
                            placeholder="Ingresar la hora final de su reserva"value="{{old("hora_cierre")}}" required>
                            @error( "hora_cierre")
                        <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>


                <div>
                    <div class="form-group">
                        <strong>Ingresar numero de telefono:</strong>
                        <input type="tel" name="cedula_test" class="form-control"
                        value="{{ old('cedula_test') }}" pattern="[+][0-5]{3}[0-9]{9}">
                        @error('cedula_test')
                            <p class="error-message">{{ $message }}</p>
                        @enderror
                    </div>
                </div>


                <div>
                    <div class="form-group">
                        <strong>Prueba 1:</strong>
                        <input type="email" name="test1_validation" class="form-control"
                            placeholder="Testeo2"value="{{old("test1_validation")}}" required>
                            @error( "test1_validation")
                        <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Prueba 2:</strong>
                        <input type="text" name="test2_validation" class="form-control"
                            placeholder="Testeo 1"value="{{old("test2_validation")}}" minlength="4">
                            @error( "test2_validation")
                        <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Prueba 3:</strong>
                        <input type="text" name="test3_validation" class="form-control"
                            placeholder="Testeo 3"value="{{old("test2_validation")}}" maxlength="6">
                            @error( "test3_validation")
                        <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>


                <div>
                    <div class="form-group">
                        <strong>Prueba 4:</strong>
                        <input type="number" name="test4_validation" class="form-control"
                            placeholder="Testeo 4"value="{{old("test2_validation")}}" max="5">
                            @error( "test4_validation")
                        <p>{{$message}}</p>
                        @enderror
                    </div>
                </div>

                

                <div class="col-xs-12 col-sm-12 col-md-12 text-center"> 
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection