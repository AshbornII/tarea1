@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Horario de Atención</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('horariosprueba.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('horariosprueba.update',$horariosprueba->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Día Atención:</strong>
                    <input type="date" name="dia_atencion" class="form-control" value="{{ $horariosprueba->dia_atencion }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Inicio Atención:</strong>
                    <input type="time" name="hora_apertura" class="form-control" value="{{ $horariosprueba->hora_apertura }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Hora Fin Atención:</strong>
                    <input type="time" name="hora_cierre" class="form-control" value="{{ $horariosprueba->hora_cierre }}">
                </div>
            </div>


            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Cedula:</strong>
                    <input type="time" name="cedula_test" class="form-control" value="{{ $horariosprueba->cedula_test }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Test 1:</strong>
                    <input type="email" name="test1_validation" class="form-control" value="{{ $horariosprueba->test1_validation }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Test 2:</strong>
                    <input type="text" name="test2_validation" class="form-control" value="{{ $horariosprueba->test2_validation }}">
                </div>
            </div>

            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Test 3:</strong>
                    <input type="text" name="test3_validation" class="form-control" value="{{ $horariosprueba->test3_validation }}">
                </div>
            </div>


            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Test 4:</strong>
                    <input type="number" name="test4_validation" class="form-control" value="{{ $horariosprueba->test4_validation }}">
                </div>
            </div>

            
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection
